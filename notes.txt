Notes
-----


3-way handshake
---------------

Host A -> B
	SYN (seq = x) - SYN bit for packet header sent
	x is first sequenced # picked by sender

Host B -> A
	SYN (seq = y, ACK = x+1)
	picks value for sequence space --- y

Host A -> B
	SYN (seq = x+1, ACK = y+1)
	this only happens if x matches up with x+1

timers on all ACKs

Tear-Down
---------
2 double handshakes

A -> B
	FIN(seq = x)

B -> A
	ACK (ACK = x+1)
	A -> B torn down when A receives this
B -> A
	FIN (seq = y)

A -> B
	ACK (ACK = y+1)
	B -> A torn down when B receives this

init funtion sets up basic structures
	timeout thread
	global structure for all connections (like class variables)
	socket port numbers to use

a connection structure for connection state:
	mutex locks to access the connection
	lists of fragments
		a list of transmitted fragments, but not acknowledged
		a list of received fragments, not read or out-of-order
	socket file descriptors for where to send the fragments

Fragment structure
	the start/end/size of the fragment
	the packet header
		in case we need to re-transmit
	the data of the fragment
	list management pointers (next/prev)

receive: recvfrom() -- waiting for connection packet
connect: create a UDP connection packet
	set up sequence number
	send a packet with SYN flag set -- use sendto()

return from accept call at end of 3 way handshake
return from connect when B -> A ACK (x + 1)
