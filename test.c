#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "sock352.h"

const char *byte_to_binary(int x) {
	static char b[9];
	b[0] = '\0';

	int z;
	for (z = 128 ; z > 0 ; z>>= 1){
		strcat(b, ((x & z) == z) ? "1" : "0");
	}

	return b;
}

int main() {

	uint8_t x = 0;
	int bit = 2;

	x |= 1 << 0;	
	x |= 1 << 1;
	x |= 1 << 2;
	x |= 1 << 3;
	x |= 1 << 4;
	x |= 1 << 5;
	x |= 1 << 6;
	x |= 1 << 7;
	
	printf("%s\n", byte_to_binary(x));
	
	int i;
	for(i = 0 ; i < 8 ; i++) {
		printf("Bit %d is: %d\n", i, (x & (1 << i)));
	}

	return 0;
}
