#include <stdlib.h>
#include <time.h>
#include <sys/types.h>

#include "sock352.h"

uint64_t rando() {
	uint64_t val;
	uint32_t x1, x2;
	srand(time(NULL));
	x1 = rand() & 0xff;
	x1 = (rand() & 0xff) << 8;
	x1 = (rand() & 0xff) << 16;
	x1 = (rand() & 0xff) << 24;

	x2 = rand() & 0xff;
	x2 = (rand() & 0xff) << 8;
	x2 = (rand() & 0xff) << 16;
	x2 = (rand() & 0xff) << 24;
	
	val = x1;
	return val << 32 | x2;
}

int sock352_init(int udp_port) {
	if(udp_port == 0) {
		return SOCK352_DEFAULT_UDP_PORT;
	}
	return udp_port;
}

int sock352_socket(int domain, int type, int protocol) {
	if(type == SOCK_STREAM) {
		if(domain == PF_CS352) {
			/* IDK WHAT TO RETURN */
			return SOCK352_SUCCESS;
		}
	}	
	/* error */
	return SOCK352_FAILURE;
}

/* DONE */
int sock352_bind(int fd, sockaddr_sock352_t *addr, socklen_t len) {
	return 0;
}

int sock352_connect(int fd, sockaddr_sock352_t *addr, socklen_t len) {
	sock352_pkt_hdr_t * pack = malloc(sizeof(sock352_pkt_hdr_t));

	struct sockaddr_in * pAddr = malloc(sizeof(struct sockaddr_in));

	pAddr->sin_family = AF_INET;
	pAddr->sin_port = addr->sin_port;
	pAddr->sin_addr = addr->sin_addr;
	
	printf("352port: %zu\n", addr->cs352_port);
	printf("UDP port: %zu\n", addr->sin_port);
	printf("in_addr: %zu\n", addr->sin_addr.s_addr);

	/* will change in later versions */
	/* when multiple ports/threads are used */
	pack->protocol = 0;
	pack->opt_ptr = 0;
	pack->source_port = 0;
	pack->dest_port = 0;

	pack->version = 0x1;
	pack->header_len = sizeof(sock352_pkt_hdr_t);	
	pack->sequence_no = rando();
	
	/* not sure about this. set SYN bit to 1 */
	pack->flags |= 1 << 1;
	
	/* send packet with initial random sequence=x */
	if(sendto(fd, pack, pack->header_len, pack->flags, pAddr, len) < 0){
		printf("sendto() failed\n");
	}

	/* receive seq=y ack=x+1 */
	// recvfrom();

	/* send ack=y+1 */
	// sendto();

	return SOCK352_FAILURE;
}

/* DONE */
int sock352_listen(int fd, int n) {
	return 0;
}

int sock352_accept(int _fd, sockaddr_sock352_t *addr, int *len) {
	sock352_pkt_hdr_t * packR = malloc(sizeof(sock352_pkt_hdr_t));
    int i = 0;
    char spinner[] = { '-', '\\', '|', '/' };
	/* receive connection packet from connect() */
	while(1) {
		if(recvfrom(_fd, packR, sizeof(sock352_pkt_hdr_t), packR->flags, addr, len) < 0) {
            i++;
            printf("\b%c",spinner[i%4]);
		}else{
			printf("flags: %d\n", packR->flags);
			return;
		}
	}

	/* send x+1 back to client */
	// sendto();

	/* receive y+1 from client */
	// recvfrom();
	
	return 0;
}

int sock352_close(int fd) {
	return -1;
}

int sock352_read(int fd, void *buf, int count) {
	return -1;
}

int sock352_write(int fd, void *buf, int count) {
	return -1;
}
	// sock352_pkt_hdr_t pack;
